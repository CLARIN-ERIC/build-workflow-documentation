= CLARIN docker build workflow documentation
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING
:ext-relative: adoc

This is the documentation describing the CLARIN ERIC docker best practices and build workflow. This currently is a work
in progress.

== Introduction

We have a number of high level goals for the CLARIN infrastructure and services:

1. Portability of the services
2. Deployability of the services
3. Evaluability of the services
4. Immutability of the infrastructure (backgound information on immutable infrastructure:
https://thenewstack.io/a-brief-look-at-immutable-infrastructure-and-why-it-is-such-a-quest/[link], https://blog.codeship.com/immutable-infrastructure/[link])

Since we are running our infrastructure on a diverse collection of hardware resources, spread over different datacenters
and hosters we want to be able to easily move service deployments around and ensure theese are as loosly coupled to
their hosts as possible. furthermore, services within the CLARIN infrastructure are often developed at external centres.
If services mature and become relevant for the community as a whole, we might decide to take over these services and run
them centrally. To ease this process a standardised package and deploy approach will improve this process.

The software developing within the CLARIN community is generally open sources and so is our infrastructure. We aim to be
able to provide examples for all services we maintain centrally so interested parties can easily run and evaluate the
services locally.

Having and immutable infrastructure greatly improves the deployability of the individual services and builds confidence
in frequently updating services and rolling back in case of issues.

This led us, the https://www.clarin.eu[CLARIN ERIC], to using https://docker.com[docker] since early 2015 to package,
deploy and manage our services. Based on this experience a set of best practices and a build workflow has been created.

A slidedeck has been made available, which was initially used to present the CLARIN ERIC docker best practices (1),
build workflow (2 and 3) and  deploy workflow (4) at a CLARIN - http://www.eurac.edu/[EURAC] meeting held in Bolzano,
Italy, April 2018:

1. https://docs.google.com/presentation/d/1-k1iWjE_bB-tO1BOHnZXetAeVPnjNm5kF_CroZo21yo/[CLARIN ERIC Infrastructure]
2. https://docs.google.com/presentation/d/1OAuJnGLEAQKL0TRzy0AyY9cjY7RVTQixl0aqGpxjA-0/[CLARIN ERIC Build Workflow]
3. https://docs.google.com/presentation/d/1B6gIJgvClR9Zs6gkmBVhmrO4hVvHQnjCd1s-cHfSA6A/[CLARIN ERIC GitLab CI Integration]
4. https://docs.google.com/presentation/d/1Wq0RWqYVRVz6Y-WQqNBVpmog8fS57MK9r37t3e_CBzc/[CLARIN ERIC Deploy Workflow]

The following sections will first discuss the best practices in more detail, then discuss the docker build workflow and
conclude with a full example showcasing the workflow.

== Docker best practices

A set of base images (images to use as a starting point) have been created, providing the basic environment, common for
a number of cases:

* https://gitlab.com/CLARIN-ERIC/docker-alpine-base[docker-alpine-base]
** https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base[docker-alpine-supervisor-base]
** https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-java-base[docker-alpine-supervisor-java-base]
** https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-java-tomcat-base[docker-alpine-supervisor-java-tomcat-base]

These images are all based on https://alpinelinux.org/[alpine linux], following the best practice set by the official
docker images. The main advantages of alpine linux are a low footprint and hardly any packages pre-installed, helping
to keep the footprint low and adding some security because there is hardly anything installed you don't want or use.

The lack of packages and the fact that the https://pkgs.alpinelinux.org/packages[alpine package repository] doesn't offer
old versions, are somethings you need to keep in mind. You have to manage most package dependencies yourself and when
you are rebuilding a docker image, you might have to update existing package versions in the dockerfile to resolve any
issues before you can rebuilt.

In order to be able to run periodic tasks, the base images include https://linux.die.net/man/8/crond[crond] as a cron
daemon, also requiring a running syslog daemon.

The following subsections will discuss the different aspects in more detail. This section is concluded with an example
to show how all the different aspects fit together by create a simple hello world container.

All crond output is configured in `/etc/fluentd/conf.d/crond.conf` and tagged with the `crond` tag.

<TODO: what is /etc/fluentd.conf/messages.conf>

=== Fluentd

One of the things we noticed when migration existing application stacks to docker images, is that services often write
logs in multiple files. Docker container only have the stdout and stderr channels, which typically output the respective
streams of the foreground process. E.g. when running tomcat in the foreground, there still are a number of log files
written to the filesystem which are relevant. In order to overcome this issue, we have decided to include a
https://www.fluentd.org/[fluentd] daemon inside our docker images which is resonpsible for tagging and aggregating log
data from multiple files into the containers stdout and stderr streams.

A specific piece of configuration is required for each application to configure this. Also, each application must ensure
logs are cleaned daily or rotated and deleted when exceeding a certain size.

All service related fluentd confguration files are place in `/etc/fluentd/conf.d/<service_name>.conf`, where
`<servicename>` should be replaced with the name of that service.

<TODO: explain fluentd tags in log output>

=== Supervisord

The base images provide an out of the box setup for http://supervisord.org/[supervisord], which is the process running
in the foreground of the container. This setup makes sure the fluentd, cron and rsyslog daemons are all running.
Supervisord is also the process running the processes for container service.

All service related supervisord confguration files are place in `/etc/supervisor/conf.d/<service_name>.conf`, where
`<servicename>` should be replaced with the name of that service.

All supervisor output is configured in `/etc/fluentd/conf.d/supervisord.conf` and  output is tagged with the `supervisord`
tag.

=== Number of processes in a container

As you can see we do not strictly follow the docker approach of running a single process per container. In reality we need
4 processes to run the basic features discussed so far and at least one more for running the actual service. Our
recommendation is to try to stick to a single service process per container, but we take a pragmatic approach. If there
is a strong reason to add more processes this should be acceptable.

For each process added to the image a specific configuration in supervisord and fluentd is required.

=== Custom initialization

The base images provide an entrypoint that will start supervisord, which in turn will make sure that all other
configured processes are started. There are cases where some custom initialization is required, before starting
the supervisord process. Restoring backups or customizing configuration are good examples.

The base image provides a hook for such custom logic in the `/app/init.sh` script. If this script exists, it will be
executed. All output of this script is tagged with the `init` tag, as configued in `/etc/fluentd/conf.d/init.sh`.

==== Customizing configuration

It is considered a best practice to build your image in such a way that a container can be started with no additional
configuration.

<TODO: explain requirement of easy evaluation>

One approach can be to setup your service configuration in such a way that the configuration files are stored in a volume
and contain placeholder values. Then you use the init script to template these configuration files if a specific file does
not exist and use sensible defaults if no custom values are supplied. After properly templating the configuration a file
is written in the volume to signal this step is not required after a restart or recreation of the container (as long as
the same configuration volume is used)

==== Restoring backups

For restoration of a backup a similar approach is taken. The backup data should be provided via a docker volume. The init
script should then have logic to decide if a restore is needed (always if the backup data is present, only if the database
is empty, whatever you want as long as it is implemented in the init script) and if this is the case, use the backup
data to run the restore.

One thing to keep in mind is that when using host mounted volumes write permission can be an issue, especially if the
docker container user is non root. In this case it could be useful to mount the backup into a temporary path and copying
it into the docker container before use as the correct user.

=== Example

To illustrate this approach a simple helloworld example has been created. The example will run one or more instances of
a simple bash script that periodically writes a "helloworld" string to log file. The script, `helloworld.sh`, looks as follows:

[source,sh]
----
#!/bin/bash
_MAX=100
_LOG_FILE=$1

SEQ=$(seq 1 ${_MAX})
for i in ${SEQ};
do
    echo "hello world, iteration: $i" >> ${_LOG_FILE}
    sleep 1
done
----

The script expects a single parameter, specifying the log file location.

<TODO: explain how to initialize the project directory>

The docker image project is structured as follows in the `image` directory:
[source,sh]
----
...
├── image
│   ├── Dockerfile
│   ├── helloworld.fluentd
│   ├── helloworld.sh
│   └── helloworld.supervisor
...
----

==== Dockerfile
Content for `./image/Dockerfile`:
[source,sh]
----
FROM registry.gitlab.com/clarin-eric/docker-alpine-supervisor-base:1.2.7
COPY helloworld.fluentd /etc/fluentd/conf.d/helloworld.conf
COPY helloworld.supervisor /etc/supervisor/conf.d/helloworld.conf
COPY helloworld.sh /helloworld.sh
RUN chmod u+x /helloworld.sh
----

This is the definition for a docker image based on the CLARIN alpine supervisor base image, providing the environment
discussed so far. In addition, the hello world script is added to image together with the supervisor and fluentd
configurations, which will be discussed in the next sections.

==== Supervisor configuration
Content for `./image/helloworld.supervisor`:
[source,sh]
----
[program:helloworld1]
command=/helloworld.sh "/log1.log"
stopwaitsecs=30

[program:helloworld2]
command=/helloworld.sh "/log2.log"
stopwaitsecs=30
----

This supervisord configuration configures two services based on the hello word script, each logging to a different log
file (`/log1.log` and `/log2.log`). Supervisord will take care of starting the scripts.

==== Fluentd configuration
Content for `./image/helloworld.fluentd`:
[source,sh]
----
<source>
  @type tail
  path /log1.log
  tag log1
  format none
</source>

<source>
  @type tail
  path /log2.log
  tag log2
  format none
</source>
----

This fluentd configuration will tail the two log files, tag all lines with `log1` or `log2` and output the aggregated
stream to the containers sdtout.

<TODO: link to the proper step in the workflow demo>

== Workflow

The goals set for our build workflow were as follows:

1. Unify build workflow across environments
2. Add support to test docker images
3. Add support to easily run docker images
4. Coupling of docker files to docker images

We are using many different environments, OSX, windows, linux and remote CI pipelines, and we want to make sure that the
build process is the same accross all of them. This could be achieved bu building the docker image inside docker. One of
the nice features of a CI pipeline is the ability to (unit) test a codebase before making a release. We would like the
ability to test our docker images before making them available. Running a docker container from an image can require some
configuration. Providing a standardized approach to run a container based on the image with sensible presets is one of
our rquiremnts. Furthermore we have experienced that the lack of a formal connection between the Dockerfile and the
resulting docker image can cause incositencies. Basically of you have a dockerfile you are free to name and tag it as you
like with each build command. Creating a tighter coupling is also one of the aspects we want the build workflow to solve.

=== Initialization

To initialize a new project for this workflow, two steps are needed:

1. Create a local directory for this project
2. Switch to this directory and initialise it as follows (if you don't trust the `curl pipe bash` approach, open the script
and run the steps manually):
[source,sh]
----
curl -s -L https://gitlab.com/CLARIN-ERIC/build-script/raw/master/init_repo.sh | bash
----

The build script is managed in it's own git repository, hosted at gitlab: https://gitlab.com/CLARIN-ERIC/build-script.

=== Project structure

A project following this workflow will be structured as follows:

[source,sh]
----
.
├── build-script-1.0.10...
├── build.sh -> build-script-1.0.10.../build.sh
├── copy_data.sh -> build-script-1.0.10.../copy_data_noop.sh
├── image
│   ├── Dockerfile
│   ├── ...
├── output
├── run
│   ├── docker-compose.yml
└── test
    └── docker-compose.yml
----

where:

* `./build-script-1.0.10...` is a directory (included a a git submodule), containing all files related to the build
environment
* `./output` is a directory to store build artifacts for CI pipelines. Not used locally.
* `./run` is directory to provide a runtime configuration example
* `./test` is a directory containing a set of test cases for this image
* `./build.sh` is a symlink to the main build script
* `./copy_data.sh` is a symlink to a noop copy data script. This can be replaced with a file containing custom copy
data logic.
* `./image` directory contains all files, including the Dockerfile, for the docker image

=== Building, testing and releasing locally

After finishing the docker image in the local `./image` directory, you can build, test and run the image locally with the
following commands:
[source,sh]
----
./build.sh --local --build
./build.sh --local --test
./build.sh --local --run
----

Building an image using this approach will result in a docker image using the projects directory name
(`PROJECT_NAME="$(basename "$(pwd)")"`) as the image name and the current git tag name, or revision otherwise, as the
image tag (`TAG="$(git describe --always)"`), resulting in `IMAGE_QUALIFIED_NAME="$PROJECT_NAME:${TAG:-latest}"`.

=== Bash aliases
Bash aliases can be used to define shortcuts for these commands:
[source,sh]
----
clarin-docker-init='curl -s -L https://gitlab.com/CLARIN-ERIC/build-script/raw/master/init_repo.sh | bash'
clarin-docker-build='sh build.sh --build --local'
clarin-docker-run='sh build.sh --run --local'
clarin-docker-test='sh build.sh --test --local'
----

=== CI Integration

The CLARIN ERIC is using GitLab to host the docker image git repositories. GitLab provides an integrated CI environment
together with a hosted docker registry. This platform allows us to automatically build, test and publish docker images,
which after being published are immutable and easy to deploy.

The GitLab CI pipeline is configured via the `./.gitlab-ci.yml` file and supports multiple stages. We typically have a
build, optional test and release stage where the build stage is run on every commit/push and the release stage is mostly
run on tags, but can be configured to run on every commit/push as well

A default CI configuration is created if you follow the initialisation approach discussed earlier and looks as follows:

[source,sh]
----
image: docker:17.05.0
services:
  - docker:17.05.0-dind

variables:
    GIT_SUBMODULE_STRATEGY: recursive

stages:
  - build
  - test
  - release

build:
  artifacts:
    untracked: true
  script: timeout -t 1440 sh -x ./build.sh --build
  stage: build
  tags:
    - docker

test:
  artifacts:
    untracked: true
  dependencies:
    - build
  script: timeout -t 1440 sh -x ./build.sh --test
  stage: test
  tags:
    - docker

release:
  artifacts:
    untracked: true
  dependencies:
    - test
  only:
    - tags
    - triggers
  script: timeout -t 1440 sh -x ./build.sh --release
  stage: release
  tags:
    - docker
----

Please note the `timeout -t 1440` configuration. This timeout prevents jobs running forever and consuming CI minutes. If
you hava long running jobs failing with `exit code 143`, consider increasing the timeout value.

With this configuration you image will be build on every push of a set of commits and the image will be tested and
released on every tag.

== Workflow demo

A demo has been prepared to showcase the whole workflow discussed so far on the following page:
link:demo_script.adoc[CLARIN docker build workflow demo]
